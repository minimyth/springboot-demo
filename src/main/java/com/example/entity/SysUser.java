package com.example.entity;

import lombok.Data;

@Data
public class SysUser{

    private String id;

    private String username;

    private String email;

    private String password;

    private Byte activated;

    private String activationkey;

    private String resetpasswordkey;

}