package com.example.service;

import com.example.entity.ResponseJson;
import com.example.exception.AjaxOperationFailException;

public interface GoodsService {

    ResponseJson buy(int goodsid,int count) throws AjaxOperationFailException;

    ResponseJson buy1(int goodsid,int count) throws AjaxOperationFailException;
}
