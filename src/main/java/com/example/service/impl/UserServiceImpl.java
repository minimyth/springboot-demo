package com.example.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import com.example.config.RedisUtil;
import com.example.entity.ResponseJson;
import com.example.mapper.UserMapper;
import com.example.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 *分页问题目前没想到
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private RedisUtil redisUtil;
    public static final String USERFOLLOW="USERFOLLOW";
    public static final String USERFANS="USERFANS";
    @Autowired
    private UserMapper userMapper;
    @Value("${server.port}")
    private String port;
    /**
     * 关注用户
     * @param uid
     * @param fid
     * @return
     */
    @Override
    public ResponseJson follow(Long uid, Long fid) {
        //新增关注
        Map map = redisUtil.getMapValue(USERFOLLOW+uid);
        if(MapUtil.isEmpty(map)){
            Map newmap = new HashMap();
            //关注的用户id作为键，时间作为值
            newmap.put(fid,System.currentTimeMillis());
            //存入redis
            redisUtil.setMapValue(USERFOLLOW+uid,newmap);
        }else {
            map.put(fid,System.currentTimeMillis());
            redisUtil.setMapValue(USERFOLLOW+uid,map);
        }

        //新增一个粉丝
        Map fans = redisUtil.getMapValue(USERFANS+fid);
        if(MapUtil.isEmpty(fans)){
            Map newmap = new HashMap();
            //fid的粉丝uid作为键，时间作为值
            newmap.put(uid,System.currentTimeMillis());
            redisUtil.setMapValue(USERFANS+fid,newmap);
        }else {
            fans.put(uid,System.currentTimeMillis());
            redisUtil.setMapValue(USERFANS+fid,fans);
        }
        return new ResponseJson("访问端口"+port+"关注成功");
    }

    /**
     * 获取关注列表
     * @param uid
     * @return
     */
    @Override
    public ResponseJson followlist(Long uid) {
        Map<String,Object> map = redisUtil.getMapValue(USERFOLLOW+uid);
        if(MapUtil.isEmpty(map)){
            return new ResponseJson(new ArrayList<>());
        }else {
            StringBuffer sb = new StringBuffer();
            for (String key : map.keySet()) {
                sb.append(key+",");
                log.info(key);
            }
            List<Map> list =  userMapper.selectByidStr("("+sb.substring(0,sb.length()-1)+")");
            for (Map map1 : list) {
                for (String key : map.keySet()) {
                    if(Integer.valueOf(map1.get("id")+"") == Integer.valueOf(key)){
                        DateTime dateTime = new DateTime(Long.valueOf(map.get(key)+""));
                        map1.put("time",dateTime);
                        log.info(map.get(key).toString()+"时间："+ dateTime);
                    }

                }
            }
            return new ResponseJson(list);
        }
    }


    /**
     * 获取粉丝列表
     * @param uid
     * @return
     */
    @Override
    public ResponseJson fanslist(Long uid) {
        Map<String,Object> map = redisUtil.getMapValue(USERFANS+uid);
        if(MapUtil.isEmpty(map)){
            return new ResponseJson(new ArrayList<>());
        }else {
            StringBuffer sb = new StringBuffer();
            for (String key : map.keySet()) {
                sb.append(key+",");
                log.info(key);
            }
            List<Map> list =  userMapper.selectByidStr("("+sb.substring(0,sb.length()-1)+")");
            for (Map map1 : list) {
                for (String key : map.keySet()) {
                    if(Integer.valueOf(map1.get("id")+"") == Integer.valueOf(key)){
                        DateTime dateTime = new DateTime(Long.valueOf(map.get(key)+""));
                        map1.put("time",dateTime);
                        log.info(map.get(key).toString()+"时间："+ dateTime);
                    }

                }
            }
            return new ResponseJson(list);
        }
    }


    /**
     * 好友列表
     * @param uid
     * @return
     */
    public ResponseJson friendslist(Long uid){
        Map<String,Object> fans = redisUtil.getMapValue(USERFANS+uid);
        Map<String,Object> follows = redisUtil.getMapValue(USERFOLLOW+uid);
        if(!MapUtil.isEmpty(fans) && !MapUtil.isEmpty(follows)){
            Set set = fans.keySet();
            Set set1 = follows.keySet();
            //去重复
            set.retainAll(set1);
            StringBuffer sb = new StringBuffer();
            for (Object key : set) {
                sb.append(key+",");
                log.info(key.toString());
            }
            List<Map> list =  userMapper.selectByidStr("("+sb.substring(0,sb.length()-1)+")");
            return new ResponseJson(list);
        }else {
            return new ResponseJson(new ArrayList<>());
        }

    }

    /**
     * 取消关注
     * @param uid
     * @param fid
     * @return
     */
    @Override
    public ResponseJson cancelfollow(Long uid, Long fid) {
        //取消关注
        Map map = redisUtil.getMapValue(USERFOLLOW+uid);
        if(MapUtil.isEmpty(map)){

        }else {
            map.remove(fid+"");
            redisUtil.setMapValue(USERFOLLOW+uid,map);
        }

        //减少一个粉丝
        Map fans = redisUtil.getMapValue(USERFANS+fid);
        if(MapUtil.isEmpty(fans)){

        }else {
            fans.remove(uid+"");
            redisUtil.setMapValue(USERFANS+fid,fans);
        }
        return new ResponseJson("取关成功");
    }
}
