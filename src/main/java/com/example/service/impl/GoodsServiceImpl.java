package com.example.service.impl;


import com.example.config.RedisUtil;
import com.example.entity.ResponseJson;
import com.example.exception.AjaxOperationFailException;
import com.example.mapper.GoodsMapper;
import com.example.service.GoodsService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.RedissonRedLock;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;


@Service
@Slf4j
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private RedisUtil redis;

    public final static  String PREFIX_LOCK_ORDER_SUBMIT = "lock_orderSubmit";

    @Autowired
    private RedissonClient redissonClient;

    /**
     * 模拟购买商品（测试高并发未加锁）
     * @param goodsid
     * @param count
     * @return
     */
    @Override
    public ResponseJson buy(int goodsid,int count) throws AjaxOperationFailException {
        RLock lock = redissonClient.getLock("stock:"+goodsid);
        // 是否还是锁定状态
        if(lock.isLocked()){
            // 时候是当前执行线程的锁
            if(lock.isHeldByCurrentThread()){
                // 释放锁
                lock.unlock();
            }
        }
        try {
            // 1. 最常见的使用方法
            //lock.lock();
            // 2. 支持过期解锁功能,10秒钟以后自动解锁, 无需调用unlock方法手动解锁
            lock.lock(10, TimeUnit.SECONDS);
            // 3. 尝试加锁，最多等待3秒，上锁以后10秒自动解锁
//            boolean res = lock.tryLock(100, 10, TimeUnit.SECONDS);
            int total = goodsMapper.selectTotal(goodsid);
            if(total<count){
                log.info("商品数量不足，购买失败！");
                throw new AjaxOperationFailException("商品数量不足，购买失败！");
            }
            int num = goodsMapper.update(goodsid,count);
            if(num>0){
                log.info("购买成功");
            }else {
                log.info("购买失败");
            }

        } finally {
            lock.unlock();
        }
        return new ResponseJson();
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    @Override
    public ResponseJson buy1(int goodsid, int count) throws AjaxOperationFailException {
        int total = goodsMapper.selectTotal1(goodsid);
        if(total<count){
            log.info("商品数量不足，购买失败！");
            throw new AjaxOperationFailException("商品数量不足，购买失败！");
        }
        int num = goodsMapper.update(goodsid,count);
        if(num>0){
            log.info("购买成功");
        }else {
            log.info("购买失败");
        }
        return new ResponseJson();
    }


}
