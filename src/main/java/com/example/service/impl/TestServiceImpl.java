package com.example.service.impl;

import com.example.entity.ResponseJson;
import com.example.mapper.TestMapper;
import com.example.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TestServiceImpl implements TestService {

    @Autowired
    private TestMapper testMapper;

    @Override
    public ResponseJson test() {
        testMapper.test();
        return new ResponseJson("操作成功");
    }

    public ResponseJson delete(int id){
        ResponseJson resp = new ResponseJson();
        return resp;
    }
}
