package com.example.service;

import com.example.entity.ResponseJson;

public interface UserService {

    ResponseJson follow(Long uid,Long fid);

    ResponseJson followlist(Long uid);

    ResponseJson fanslist(Long uid);

    ResponseJson friendslist(Long uid);

    ResponseJson cancelfollow(Long uid, Long fid);
}
