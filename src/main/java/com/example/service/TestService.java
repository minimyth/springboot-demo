package com.example.service;

import com.example.entity.ResponseJson;

public interface TestService {

    ResponseJson test();

    ResponseJson delete(int id);
}
