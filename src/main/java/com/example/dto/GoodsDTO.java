package com.example.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GoodsDTO {

    @NotNull(message = "商品id不能为空")
    private Integer goodsid;

    @NotNull(message = "商品数量不能为空")
    private  Integer count;



    public static void main(String[] args) {
        GoodsDTO goods = new GoodsDTO();
        goods.setGoodsid(1);
        goods.setCount(100);
        goods.goodschange(goods);
        System.out.println(goods.toString());

    }


    /**
     * 引用传递（传递的是对象引用的拷贝，方法对引用指向的对象进行了操作，原始对象就发生了改变）
     * @param goods
     */
    public void goodschange(GoodsDTO goods){
        goods.setCount(99);
        System.out.println(goods.toString());
    }

}
