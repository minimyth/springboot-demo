package com.example.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class User implements Serializable {
    @NotNull(message = "用户id不能为空")
    private int id;
    @NotBlank(message = "用户账号不能为空")
    @Size(min = 6, max = 11, message = "账号长度必须是6-11个字符")
    private String name;
    @NotBlank(message = "用户电话不能为空")
    @Size(min = 11, max = 11, message = "电话长度必须是11个字符")
    private String phone;
    @NotNull(message = "菜单数组不能为空")
    private int[] menuid;

}
