package com.example.controller;

import com.example.entity.ResponseJson;
import com.example.dto.User;
import com.example.exception.AjaxOperationFailException;
import com.example.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;


@RequestMapping("api/test")
@RestController
@Slf4j
public class TestController {

    @Autowired
    private TestService testService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RequestMapping("/insert")
    public ResponseJson insert(){
        return testService.test();
    }

    @PostMapping("/post")
    public String test( @RequestBody @Validated User user){
        log.error("测试。。。。。。。。。。。。。。。。。。。。。");
        return user.toString();
    }

    @GetMapping("/get/{id}")
    public ResponseJson test2(@PathVariable @Validated @NotNull Integer id){
        return new ResponseJson("hello get请求传来参数"+id);
    }

    @PostMapping("/post1")
    public ResponseJson test1( @RequestBody @Validated User user) throws AjaxOperationFailException {
        if(1==1){
            throw new AjaxOperationFailException("查找的用户不存在");
        }
        return new ResponseJson(user);
    }

    @GetMapping("/get")
    public ResponseJson test3(@RequestParam("id") @NotNull (message = "用户id不能为空") int id){
        return new ResponseJson("hello get请求传来参数"+id);
    }

    @RequestMapping("/sendmsg")
    public ResponseJson sendmsg( @RequestBody @Validated User user){
        log.info("发送消息到队列fisrt");
        for (int i = 0; i < 10000; i ++){
            user.setId(i);
            rabbitTemplate.convertAndSend("topicExchange","topic.first",user);
        }
        return new ResponseJson(user);
    }

    @PostMapping("/sendmsg2")
    public ResponseJson sendmsg2( @RequestBody @Validated User user) throws AjaxOperationFailException {
        rabbitTemplate.convertAndSend("topicExchange","topic.second",user);
        return new ResponseJson(user);
    }


    @DeleteMapping("/delete")
    public ResponseJson delete(@RequestParam("id") @NotNull  int id){
        return testService.delete(id);
    }


    public static void main(String[] args) {
        com.example.entity.User user = new com.example.entity.User();
        user.setId(1L);
        user.setName("tom");
        int i = 100;
        Integer j = 1000;
        System.out.println(user.toString()+"<"+i+">"+"<"+j+">");
        setUser(user,i,j);
        System.out.println(user.toString());
        System.out.println(user.toString()+"<"+i+">"+"<"+j+">");
    }


    /**
     * 传递是对象得引用（修改的是引用对应的对象，而不是引用本身）
     * @param user
     */
    public static void setUser(com.example.entity.User user,int i,Integer j){
        user.setName("jack");
        i=101;
        j=1001;
        System.out.println(user.toString());
        System.out.println(user.toString()+"<"+i+">"+"<"+j+">");
    }

}
