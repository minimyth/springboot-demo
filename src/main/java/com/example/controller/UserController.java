package com.example.controller;

import com.example.entity.ResponseJson;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequestMapping("api/user")
@RestController
public class UserController {

    @Autowired
    private UserService userService;



    /**
     * 关注
     * @param uid
     * @param fid
     * @return
     */
    @PostMapping("/follow")
    public ResponseJson follow(Long uid,Long fid){
        return userService.follow(uid,fid);
    }

    /**
     * 取消关注
     * @param uid
     * @param fid
     * @return
     */
    @PostMapping("/cancelfollow")
    public ResponseJson cancelfollow(Long uid,Long fid){
        return userService.cancelfollow(uid,fid);
    }



    /**
     * 关注列表
     * @param uid
     * @return
     */
    @PostMapping("/followlist")
    public ResponseJson followlist(Long uid){
        return userService.followlist(uid);
    }

    /**
     * 获取粉丝列表
     * @param uid
     * @return
     */

    @PostMapping("/fanslist")
    public ResponseJson fanslist(Long uid) {
        return userService.fanslist(uid);
    }


    /**
     * 好友
     * @param uid
     * @return
     */
    @PostMapping("/friendslist")
    public ResponseJson friendslist(Long uid) {
        return userService.friendslist(uid);
    }



}
