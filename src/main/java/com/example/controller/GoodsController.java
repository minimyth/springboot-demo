package com.example.controller;

import com.example.dto.GoodsDTO;
import com.example.entity.ResponseJson;
import com.example.exception.AjaxOperationFailException;
import com.example.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/goods/")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;
    /**
     * 模拟购买商品（redis分布式锁 测试高并发）
     * @return
     */
    @PutMapping("/buy")
    public ResponseJson buy(@RequestBody @Validated GoodsDTO goods) throws AjaxOperationFailException {
        return goodsService.buy(goods.getGoodsid(),goods.getCount());
    }


    /**
     *模拟购买商品（mysql行锁 测试高并发）
     * @param goods
     * @return
     * @throws AjaxOperationFailException
     */
    @PutMapping("/buy1")
    public ResponseJson buy1(@RequestBody @Validated GoodsDTO goods) throws AjaxOperationFailException {
        return goodsService.buy1(goods.getGoodsid(),goods.getCount());
    }

}
