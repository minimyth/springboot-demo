package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("acp")
public class TestHtmlController {


    /**
     * 测试第一个页面
     * @return
     */
    @RequestMapping("/test1")
    public String test1(){
        return "test1";
    }

    @RequestMapping("/index1")
    public String index1(){
        return "index1";
    }
}
