package com.example.controller;

import com.example.entity.ResponseJson;
import com.example.exception.AjaxOperationFailException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
@Slf4j
public class BaseController {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseJson MethodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        // 从异常对象中拿到ObjectError对象
        ObjectError objectError = e.getBindingResult().getAllErrors().get(0);
        // 然后提取错误提示信息进行返回
        return new ResponseJson(1001,objectError.getDefaultMessage());
    }


    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseJson handException(HttpServletRequest req, HttpServletResponse resp, Exception e) {
        ResponseJson responseJson = new ResponseJson();
        responseJson.setCode(1004);
        if (e instanceof AjaxOperationFailException) {
            responseJson.setMsg(((AjaxOperationFailException) e).getErrorMessage());
        } else {
            log.error("error信息：",e);
            responseJson.setMsg("服务器繁忙，请稍后再试。。。");
        }
        return responseJson;
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseJson MethodArgumentNotValidExceptionHandler(MethodArgumentTypeMismatchException e) {
        // 然后提取错误提示信息进行返回
        System.out.println("参数异常:{}"+(Object) e);
        return new ResponseJson(1002,"参数类型不匹配");
    }


    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseJson MissingServletRequestParameterException(MissingServletRequestParameterException e) {
        // 然后提取错误提示信息进行返回
        System.out.println("参数异常:{}"+(Object) e);
        return new ResponseJson(1002,"参数缺失");
    }



    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseJson HttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        // 然后提取错误提示信息进行返回
        System.out.println("参数异常:{}"+(Object) e);
        return new ResponseJson(1005,"请求方式错误，请仔细看文档");
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseJson HttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException e) {
        // 然后提取错误提示信息进行返回
        System.out.println("参数异常:{}"+(Object) e);
        return new ResponseJson(1005,"请求体错误，请仔细看文档");
    }

}
