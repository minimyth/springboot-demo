package com.example.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@ServerEndpoint(value = "/connectWebSocket")
public class WebSocket {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * 会话
     */
    private Session session;

    private static Map<String, WebSocket> clients = new ConcurrentHashMap<String, WebSocket>();
    /**
     * 建立连接
     *
     * @param session
     */
    @OnOpen
    public void onOpen(Session session)
    {
        this.session = session;
        logger.info("新用户接入。。。。。。。。。。。。。。。。。。。。。。");
        clients.put(session.getId(), this);

    }

    @OnError
    public void onError(Session session, Throwable error) {
        logger.info("服务端发生了错误"+error.getMessage());
    }
    /**
     * 连接关闭
     */
    @OnClose
    public void onClose()
    {
        clients.remove(session.getId(), this);
    }

    /**
     * 收到客户端的消息
     *
     * @param message 消息
     * @param session 会话
     */
    @OnMessage
    public void onMessage(String message, Session session)
    {

    }


    public void sendMessageAll(String message) throws IOException {
        if(clients.size()==0){
            return;
        }
        logger.info("向"+clients.size()+"个客户端推送消息");
        for (WebSocket item : clients.values()) {
            if(item.session.isOpen()){
                item.session.getAsyncRemote().sendText(message);
            }
        }
    }

}
