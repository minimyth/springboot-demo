package com.example.rabiitmq;

import com.example.dto.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "topic.second")
@Slf4j
public class TopicReceiver2 {

    @RabbitHandler
    public void process(User testMessage) {
//        log.info("TopicReceiver2消费者收到消息  : " + testMessage.toString());
    }
}
