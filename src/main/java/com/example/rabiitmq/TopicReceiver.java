package com.example.rabiitmq;

import com.example.dto.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "topic.first")
@Slf4j
public class TopicReceiver {

    @RabbitHandler
    public void process(User testMessage) {
//        log.info("TopicReceiver消费者收到消息  : " + testMessage.toString());
    }
}
