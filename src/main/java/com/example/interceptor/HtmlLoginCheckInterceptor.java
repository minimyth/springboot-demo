package com.example.interceptor;

import cn.hutool.json.JSONUtil;
import com.example.entity.ResponseJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class HtmlLoginCheckInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse resp, Object o) throws Exception {
        String url = req.getRequestURI();
        log.debug("请求来了:"+url);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        if (modelAndView == null) {
            return;
        }

        ModelMap mm = modelAndView.getModelMap();
        if (mm == null) {
            return;
        }

        String requestUrl = httpServletRequest.getRequestURI();
        if (!requestUrl.contains(".html")) {
            return;
        }

    }
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }

    private void writeJsonOutput(HttpServletResponse resp, int type) throws IOException {
        ResponseJson responseResult = new ResponseJson();
        if (type == 0) {
            responseResult.setCode(101);
            responseResult.setMsg("请登录后再操作！");
        }else if (type == 1) {
            responseResult.setCode(103);
            responseResult.setMsg("缺失参数token！");
        }else {
            responseResult.setCode(102);
            responseResult.setMsg("对不起，您没有权限进行此操作！");
        }
        resp.setContentType("application/json");
        resp.setCharacterEncoding("utf-8");
        resp.getWriter().write(JSONUtil.toJsonStr(responseResult));
    }




}
