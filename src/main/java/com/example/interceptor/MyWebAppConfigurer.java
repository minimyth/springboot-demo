package com.example.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@Component
@Slf4j
public class MyWebAppConfigurer implements WebMvcConfigurer {

    @Autowired
    private HtmlLoginCheckInterceptor htmlLoginCheckInterceptor;

    @Autowired
    private OriginCheckInterceptor originCheckInterceptor;


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 多个拦截器组成一个拦截器链
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
        log.info("进入拦截器");
        registry.addInterceptor(originCheckInterceptor).addPathPatterns("/**");
        registry.addInterceptor(htmlLoginCheckInterceptor).addPathPatterns("/api/**");
        registry.addInterceptor(htmlLoginCheckInterceptor).addPathPatterns("/acp/**");
    }
}