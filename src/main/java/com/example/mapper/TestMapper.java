package com.example.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;

public interface TestMapper {

    @Insert("insert into demo values(1,'myth')")
    void test();

    @Delete("delete from admin where id = #{id}")
    int delete(int id);
}
