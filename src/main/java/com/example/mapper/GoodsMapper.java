package com.example.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface GoodsMapper {


    @Update("update goodstotal set total=total-#{count} where goodsid=#{goodsid}")
    int update(@Param("goodsid") int goodsid,@Param("count") int count);

    @Select("select total from goodstotal where goodsid=#{goodsid}")
    int selectTotal(int goodsid);

    @Select("select total from goodstotal where goodsid=#{goodsid} for update")
    int selectTotal1(int goodsid);
}
