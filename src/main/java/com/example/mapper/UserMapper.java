package com.example.mapper;

import com.example.entity.User;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

public interface UserMapper {
    @Delete({
        "delete from yuser",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into yuser (name, sex)",
        "values (#{name,jdbcType=VARCHAR}, #{sex,jdbcType=INTEGER})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=Long.class)
    int insert(User record);

    @Select({
        "select",
        "id, name, sex",
        "from yuser",
        "where id = #{id,jdbcType=BIGINT}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="sex", property="sex", jdbcType=JdbcType.INTEGER)
    })
    User selectByPrimaryKey(Long id);

    @Select({
        "select",
        "id, name, sex",
        "from yuser"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="sex", property="sex", jdbcType=JdbcType.INTEGER)
    })
    List<User> selectAll();

    @Update({
        "update yuser",
        "set name = #{name,jdbcType=VARCHAR},",
          "sex = #{sex,jdbcType=INTEGER}",
        "where id = #{id,jdbcType=BIGINT}"
    })
    int updateByPrimaryKey(User record);

    @Select("SELECT * from yuser where id in ${str}")
    List<Map> selectByidStr(@Param("str") String str);
}