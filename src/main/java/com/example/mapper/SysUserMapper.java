package com.example.mapper;

import com.example.entity.SysUser;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface SysUserMapper {
    @Delete({
        "delete from sys_user",
        "where id = #{id,jdbcType=CHAR}"
    })
    int deleteByPrimaryKey(String id);

    @Insert({
        "insert into sys_user (username, email, ",
        "password, activated, ",
        "activationkey, resetpasswordkey)",
        "values (#{username,jdbcType=VARCHAR}, #{email,jdbcType=VARCHAR}, ",
        "#{password,jdbcType=VARCHAR}, #{activated,jdbcType=TINYINT}, ",
        "#{activationkey,jdbcType=VARCHAR}, #{resetpasswordkey,jdbcType=VARCHAR})"
    })
    @SelectKey(statement="SELECT LAST_INSERT_ID()", keyProperty="id", before=false, resultType=String.class)
    int insert(SysUser record);

    @Select({
        "select",
        "id, username, email, password, activated, activationkey, resetpasswordkey",
        "from sys_user",
        "where id = #{id,jdbcType=CHAR}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.CHAR, id=true),
        @Result(column="username", property="username", jdbcType=JdbcType.VARCHAR),
        @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
        @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
        @Result(column="activated", property="activated", jdbcType=JdbcType.TINYINT),
        @Result(column="activationkey", property="activationkey", jdbcType=JdbcType.VARCHAR),
        @Result(column="resetpasswordkey", property="resetpasswordkey", jdbcType=JdbcType.VARCHAR)
    })
    SysUser selectByPrimaryKey(String id);

    @Select({
        "select",
        "id, username, email, password, activated, activationkey, resetpasswordkey",
        "from sys_user"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.CHAR, id=true),
        @Result(column="username", property="username", jdbcType=JdbcType.VARCHAR),
        @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
        @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
        @Result(column="activated", property="activated", jdbcType=JdbcType.TINYINT),
        @Result(column="activationkey", property="activationkey", jdbcType=JdbcType.VARCHAR),
        @Result(column="resetpasswordkey", property="resetpasswordkey", jdbcType=JdbcType.VARCHAR)
    })
    List<SysUser> selectAll();

    @Update({
        "update sys_user",
        "set username = #{username,jdbcType=VARCHAR},",
          "email = #{email,jdbcType=VARCHAR},",
          "password = #{password,jdbcType=VARCHAR},",
          "activated = #{activated,jdbcType=TINYINT},",
          "activationkey = #{activationkey,jdbcType=VARCHAR},",
          "resetpasswordkey = #{resetpasswordkey,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=CHAR}"
    })
    int updateByPrimaryKey(SysUser record);
}