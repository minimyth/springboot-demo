package com.example;

import com.example.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@MapperScan(basePackages = "com.example.mapper")
@ServletComponentScan
@Slf4j
class SpringbootDemoApplicationTests {

    @Autowired
    private TestService testService;

    @Test
    void contextLoads() {
        log.info("执行test测试");
        testService.test();
    }

}
